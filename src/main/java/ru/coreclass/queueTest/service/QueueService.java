package ru.coreclass.queueTest.service;

import org.springframework.stereotype.Service;
import ru.coreclass.queueTest.model.Employee;

import java.util.*;

@Service
public class QueueService {
    public void buildQueue(String[] people) {
        /*
         * Инициализируем кассы
         * Временные значения округлены до секунд
         */
        // 10 в час
        Employee employee1 = Employee.builder().id(1).queue(0).maxDelay(7200).delay(0).incrementFactor(360).build();
        // 13 в час
        Employee employee2 = Employee.builder().id(2).queue(0).maxDelay(5520).delay(0).incrementFactor(276).build();
        // 15 в час
        Employee employee3 = Employee.builder().id(3).queue(0).maxDelay(4800).delay(0).incrementFactor(240).build();
        // 17 в час
        Employee employee4 = Employee.builder().id(4).queue(0).maxDelay(4220).delay(0).incrementFactor(211).build();
        // Создаем ArrayList в порядке приоритета
        ArrayList<Employee> employees = new ArrayList<>(Arrays.asList(employee4, employee3, employee2, employee1));

        List<Integer> queue = new ArrayList<>();

        for(int i = 0; i < people.length; i++)
            queue.add(assignEmployees(employees));

        int i = 0;

        System.out.print("output: ");
        Iterator<Integer> queueIterator = queue.iterator();
        while(queueIterator.hasNext())
            System.out.print(queueIterator.next() + " ");
        System.out.println("\n");
    }

    private Integer assignEmployees(ArrayList<Employee> employees) {
        Integer currentDelay = 8000;
        Integer currentEmployee = 0;
        for (Employee employee : employees) {
            if(employee.getDelay().equals(0)) {
                assignEmployee(employee);
                return employee.getId();
            }
            if(employee.getMaxDelay() > employee.getDelay() && currentDelay > employee.getDelay()) {
                currentDelay = employee.getDelay();
                currentEmployee = 4 - employee.getId();
            }
        }
        assignEmployee(employees.get(currentEmployee));
        return employees.get(currentEmployee).getId();
    }

    private void assignEmployee(Employee employee) {
        employee.setQueue(employee.getQueue() + 1);
        employee.setDelay(employee.getDelay() + employee.getIncrementFactor());
    }
}
