package ru.coreclass.queueTest.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Employee {
    private Integer id;
    private Integer maxDelay;
    private Integer delay;
    private Integer queue;
    private Integer incrementFactor;
}
