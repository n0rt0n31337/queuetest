package ru.coreclass.queueTest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.coreclass.queueTest.service.QueueService;

import java.util.Arrays;
import java.util.Scanner;

@SpringBootApplication
public class Application implements CommandLineRunner {
    private QueueService queueService;

    @Autowired
    private void setApplicationParams(QueueService queueService) {
        this.queueService = queueService;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.print("input:  ");
        String people[] = scanner.nextLine().split(" ");
        scanner.close();
        if(people.length > 80)
            throw new Exception("Limit exceeded");
        queueService.buildQueue(people);
    }
}